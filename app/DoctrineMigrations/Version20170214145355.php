<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170214145355 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('create table error (
                                id int auto_increment not null, 
                                word_id int not null, 
                                primary key(id)) 
                                default character set utf8 collate utf8_unicode_ci engine = InnoDB');
         
         $this->addSql('alter table error add constraint error_word_id_to_word_word_id foreign key (word_id) references word(id)');
     }
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
       $this->addSql('alter table error drop foreign key`error_word_id_to_word_word_id`');
       $this->addSql('drop table error');
       
    }
}
