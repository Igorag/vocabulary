<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170214143630 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
       
        $this->addSql('create table word (
                                id int auto_increment not null, 
                                word_eng varchar(50) not null unique, 
                                word_rus varchar(50) not null unique, 
                                primary key(id)) 
                                default character set utf8 collate utf8_unicode_ci engine = InnoDB');

        $json_data = '{
        "blackberry": "ежевика",
        "grape": "виноград",
        "apple": "яблоко",
        "orange": "апельсин",
        "pear": "персик",    
        "pineapple": "ананас",
        "watermelon": "арбуз",
        "lemon": "лимон",
        "coconut": "кокос",
        "raspberry": "малина",
        "banana": "банан",
        "pomelo": "помело",
        "strawberry": "клубника",
        "mango": "манго",
        "melon": "дыня",
        "apricot": "абрикос",
        "pomegranate": "гранат",
        "pear": "слива",
        "cherry": "вишня",
        "bilberry": "черника"
       }';
       
        $words = json_decode ($json_data);
        foreach ($words as $k => $v) {
            $this->addSql('insert into word (word_eng, word_rus) values("'.$k.'", "'.$v.'")');
        }
                                
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('drop table word');

    }
}
