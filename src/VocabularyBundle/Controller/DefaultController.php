<?php

namespace VocabularyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use VocabularyBundle\Entity\Result;
use VocabularyBundle\Entity\Error;

use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{    
    /**
     * @Route("/vocabulary/index")
     */
    public function indexAction()
    {
        return $this->render('VocabularyBundle:Default:index.html.twig');
    }
    
    /**
     * @Route("/vocabulary/test")
     */
     // Переход на страницу для тестирования
    public function testAction()
    {
        return $this->render('VocabularyBundle:Default:test.html.twig');
    }

    // Количество слов в таблице word (максимальное количество вопросов/ответов)
    public function wordcountAction()
    {  
        $em = $this->getDoctrine()->getManager();
        
        $repoWord = $em->getRepository('VocabularyBundle:Word');
        $queryWord= $repoWord->createQueryWordCount();
        $resWordCount = $queryWord->getSingleScalarResult();
        
        //
        return new Response($resWordCount);
    }
    
    // Вопрос для проверки
    public function questionAction()
    {  
        $em = $this->getDoctrine()->getManager();        
        $repoWord = $em->getRepository('VocabularyBundle:Word');
        $queryWord= $repoWord->createQueryWordCount();
        $resWordCount = $queryWord->getSingleScalarResult();
        
        // Количество пар слов в таблице word
        $word_table_size = $resWordCount;
        
        // Все id таблицы word
        for ($i = 1; $i <= $word_table_size; $i++) $all_word_id[$i] = $i;
        
        // Со страницы - тест в первый раз? Или тест заново сначала? Т.е первый вопрос теста, все слова должны быть доступны
        // Тогда $post_data->start_test = true, массив "использованных" слов "обнуляется, следующие вопросы - этот массив пополняется новыми id
        $post = $this->getRequest()->getContent();
        $post_data = json_decode($post);                
        $start_test = $post_data->start_test;
        
        $session = $this->getRequest()->getSession();
        
        // $used_word_id - "использованные" слова
        if ($start_test == true) {
            $used_word_id = [];
            $session->set('used_word_id', $used_word_id);
        } else $used_word_id  = (array) $session->get('used_word_id');
        
        // Разность (какие слова еще не были использованы в текущем тесте, массив $not_used_word_id)
        $not_used_word_id  =array_diff($all_word_id, $used_word_id);
        sort($not_used_word_id); 

        // id очередного слова случайно
       $random_next_word_id = $not_used_word_id[mt_rand(0, count($not_used_word_id)-1)];  
       
       // Сохранение в сессии "нового" id
       array_push($used_word_id, $random_next_word_id);
       $session->set('used_word_id', $used_word_id);
       
       $used_word_id1  = (array) $session->get('used_word_id');     
        
        // Массив случайных id для вариантов ответа (1+3 = 4)
        $rnd_ids [] = $random_next_word_id;
        while (count($rnd_ids) < 4) {
            $new_rnd = mt_rand(1,  $word_table_size);
            // Такого id в массиве еще нет и очередной id не $random_next_word_id - добавить к случайным ответам
            if (!in_array($new_rnd, $rnd_ids) and $new_rnd <> $random_next_word_id) $rnd_ids[]=$new_rnd;
        }
     
        // Выборка случайных пар слов
        $queryWordQuestion= $repoWord->createQueryWordQuestion($rnd_ids);
        $resWordQuestion = $queryWordQuestion->getResult();
        
        // Дополнительное "перемешивание" 
        // (лишнее/нелишнее, слово для проверки может быть быть в любом месте среди других слов-случайных вариантов ответа,
        // дополнительное "перемешивание")
        shuffle($resWordQuestion);        
            
        // Показать английское или русское слово? (1-eng, 0-rus)
        $eng_rus=mt_rand(0,1);
        
        // Слово для "вопроса"
        foreach ($resWordQuestion as $k => $v)
            if ($v->getId() == $random_next_word_id) {
                if ($eng_rus == 1) $word_test = $v->getWordEng(); else $word_test = $v->getWordRus();
            }
        
        // "Вопрос"  
        foreach ($resWordQuestion as $k => $v)
             if ($eng_rus == 1) $question[] = ['id' => $v->getId(), 'word_variant' => $v->getWordRus()];
             else $question[] = ['id' => $v->getId(), 'word_variant' => $v->getWordEng()];

        // Передача на страницу
        return new Response(json_encode([$eng_rus, $word_test, $question]));
    }
    
    // Проверка ответа
    public function checkAction()
    {
        $post = $this->getRequest()->getContent();
        $post_data = json_decode($post);
        //        
        $id = $post_data->id;

        // Из сессии - массив "использованных" слов (id ответа - последний элемент)
        $session = $this->getRequest()->getSession();

        $used_word_id  = (array) $session->get('used_word_id'); 
        // Проверка - совпадение/несовпадение
        if ($used_word_id[count($used_word_id)-1] == $id) {
            return new Response(json_encode([true]));
        } else {
               // Запись ошибки     
               $em = $this->getDoctrine()->getManager();           
               $entity = new Error();
               $entity->setWordId((int)$id);
               
               $em->persist($entity);
               $em->flush();
               //
               return new Response(json_encode([false]));
           }
    }
    
    // Запись результата теста
    public function writeresultAction()
    {
        $post = $this->getRequest()->getContent();
        $post_data = json_decode($post);                

        //        
        $username = $post_data->username;
        $result = $post_data->result;
        //
        $em = $this->getDoctrine()->getManager();
        //               
        $entity = new Result();
        $entity->setUsername($username);
        $entity->setResult($result);
        $em->persist($entity);
        $em->flush();

        return  new Response();
    }
            
}
