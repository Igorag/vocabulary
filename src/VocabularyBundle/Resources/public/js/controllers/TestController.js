testApp.controller('TestController',
    function TestController($scope, $http){
        
        $scope.mode = 'start';
        $scope.username = '';
        $scope.error_count = 0;
        $scope.result = 0;
        $scope.message = '';
        $scope.start_test = true;

        $scope.word_variant = '';
        $scope.eng_rus = -1;
        $scope.max_word = 0;
        $scope.word_rus = '';
        $scope.word_eng = '';
               
        $scope.block_visible = {visible: false};
        
        $scope.begin_test = function (username) {
            $scope.mode = 'test';
            $scope.username = username;
            $scope.result = 0;
            $scope.error_count = 0;
            $scope.start_test = true;

            //
            finish_message = angular.element(document.querySelector(".finish_message"));
            finish_message.text('');
            $scope.message = '';
            
            // 
            $scope.question();
            $scope.wordcount();

        };
                
        $scope.question = function () {
              $scope.block_visible.visible = false;         
              //
              $http({
                      method: 'POST', 
                      url: '../vocabulary/question',
                      data:{'start_test': $scope.start_test}
                  })
                  .then(function onSuccess(response) {
                      $scope.start_test = false;
                      
                      $scope.next_question = response.data;                      
                      
                      $scope.eng_rus = $scope.next_question[0];
                      $scope.word_test= $scope.next_question[1];
                      $scope.modes=$scope.next_question[2]; 
                      //
                      if ($scope.eng_rus == 1) $scope.word_eng = $scope.word_test; else  $scope.word_rus = $scope.word_test;
                      
                      //
                      $scope.block_visible.visible = true;
                      
                  }).catch(function onError(response) {
                            console.log(response);
                            //  Обработка ошибки
                     });
        };

        // Проверка ответа
        $scope.check_answer = function (answer_id, word_variant) {
              $scope.block_visible.visible = false;
              
              if ($scope.eng_rus == 1) $scope.word_rus = word_variant; else  $scope.word_eng = word_variant;
                   
              var params = {'id': answer_id};
                 
              $http({
                      method: 'POST', 
                      url:'../vocabulary/check', 
                      data: params
                  })
                  .then(function onSuccess(response) {
                      $scope.check = response.data[0];
                      if ($scope.check==true) { // Ответ правильный
                          $scope.result++;

                           var message = angular.element(document.querySelector(".message"));
                           if (message.hasClass('answer_wrong')) message.removeClass('answer_wrong'); 
                           message.addClass('answer_true');                          
                           $scope.message = $scope.word_eng+' - '+$scope.word_rus+': ответ правильный :)';
                          
                          // Проверка не пора ли закончить тест
                          if ($scope.max_word == $scope.result+$scope.error_count) {     
                             $scope.writeresult(); // Запись результата теста
                              var finish_message_text = $scope.word_eng+' - '+$scope.word_rus+': ответ правильный. Больше нет слов для теста.Тест закончен. Количество баллов: '+$scope.result+' количество ошибок '+$scope.error_count;
                              //
                              var finish_message = angular.element(document.querySelector(".finish_message"));
                              if (finish_message.hasClass('answer_wrong')) message.removeClass('answer_wrong'); 
                              finish_message.addClass('answer_true')
                              finish_message.text(finish_message_text);                              
                              
                              $scope.mode = 'start'; // Возможность начать новый тест
                              //
                          } else $scope.question($scope.start_test); 
                      } else { // Ответ неправильный
                             $scope.error_count++;
                             
                             var message = angular.element(document.querySelector(".message"));                             
                             if (message.hasClass('answer_true')) message.removeClass('answer_true');
                             message.addClass('answer_wrong');
                             $scope.message = $scope.word_eng+' - '+$scope.word_rus+': ответ неправильный (:';
                             //
                             if ($scope.error_count == 3) {
                                 $scope.writeresult(); // Запись результата теста
                                 //
                                 var finish_message_text = $scope.word_eng+' - '+$scope.word_rus+': ответ неправильный. Сделано 3 ошибки (допустимо не более 2-х). Тест завершен. Количество баллов: '+$scope.result+' количество ошибок '+$scope.error_count;
                                 var finish_message = angular.element(document.querySelector(".finish_message")); 
                                 if (finish_message.hasClass('answer_true')) message.removeClass('answer_true');
                                 finish_message.addClass('answer_wrong');
                                 finish_message.text(finish_message_text);
                                 
                                 $scope.mode = 'start'; // Возможность начать новый тест
                             } else $scope.question($scope.start_test); //Иначе следующий вопрос
                         }                      
                  }).catch(function onError(response) {
                          console.log(response);
                          //  Обработка ошибки
                     });   
        };
        
        // Количество слов = максимальное количество попыток
        $scope.wordcount = function () {
              $http({method: 'POST', url:'../vocabulary/wordcount'})
                  .then(function onSuccess(response) {
                       $scope.max_word=response.data;
                  }).catch(function onError(response) {
                          console.log(response);
                     });
        };
        
        // Запись результата теста
        $scope.writeresult = function () {
              $http({
                      method: 'POST', 
                      url: '../vocabulary/writeresult', 
                      data: {'username': $scope.username, 'result': $scope.result}
                  })
                  .then(function onSuccess(response) {
                                           
                  }).catch(function onError(response) {
                           console.log(response);
                           //  Обработка ошибки
                     });
        };        
    }
)