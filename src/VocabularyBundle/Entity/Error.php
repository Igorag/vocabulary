<?php

namespace VocabularyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Error
 *
 * @ORM\Table(name="error")
 * @ORM\Entity(repositoryClass="VocabularyBundle\Repository\ErrorRepository")
 */
class Error
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="word_id", type="integer")
     */
    private $wordId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wordId
     *
     * @param integer $wordId
     *
     * @return Error
     */
    public function setWordId($wordId)
    {
        $this->wordId = $wordId;

        return $this;
    }

    /**
     * Get wordId
     *
     * @return int
     */
    public function getWordId()
    {
        return $this->wordId;
    }
}

