<?php

namespace VocabularyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Word
 *
 * @ORM\Table(name="word")
 * @ORM\Entity(repositoryClass="VocabularyBundle\Repository\WordRepository")
 */
class Word
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="word_eng", type="string", length=50)
     */
    private $wordEng;

    /**
     * @var string
     *
     * @ORM\Column(name="word_rus", type="string", length=50)
     */
    private $wordRus;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wordEng
     *
     * @param string $wordEng
     *
     * @return Word
     */
    public function setWordEng($wordEng)
    {
        $this->wordEng = $wordEng;

        return $this;
    }

    /**
     * Get wordEng
     *
     * @return string
     */
    public function getWordEng()
    {
        return $this->wordEng;
    }

    /**
     * Set wordRus
     *
     * @param string $wordRus
     *
     * @return Word
     */
    public function setWordRus($wordRus)
    {
        $this->wordRus = $wordRus;

        return $this;
    }

    /**
     * Get wordRus
     *
     * @return string
     */
    public function getWordRus()
    {
        return $this->wordRus;
    }
}

