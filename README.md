Vocabulary - Symfony Standard Edition
======================

Welcome to Vocabulary based on Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

Vocabulary
======

Пример кода - словарь - простая проверка знания слов

Технолоии - Symfony 2 + AngularJS


Как развернуть 
==========

1. Склонировать проект

2. Создать БД

3. Открыть консоль, перейти в корневой каталог проекта и выполнить команду

    composer update
    
    (ответить на вопросы: БД, пользователь/пароль)
    
4. Применить миграции консольной командой 

    php app/console doctrine:migrations:migrate 
    
    (создаются таблицы в БД, таблица слов заполняется словами)
    
5. Страница с тестом - адрес подобный такому адресу (apache):

        http://localhost/ПАПКА_ПРОЕКТА/web/app_dev.php/vocabulary/test
        
        http://localhost/ПАПКА ПРОЕКТА/web/app.php/vocabulary/test

